# huertotronico

Sensado de condiciones ambientales en PILARES Pantitlan con arduinos para 
reconocer el efecto de las mismas en el rendimiento de diversas hortalizas. 

## Objetivo general. 
Generar una variedad de fichas tecnicas que ayuden a la produccion de hortalizas en espacios urbanos de la region

## Objetivo particular. 
1. Sensar de forma cuantitativa las condiciones de humedad, temperatura, horas luz 
1. Valorar el rendimiento kg(m-2)
1. Analisis de los resultados y redaccion de fichas tecnicas 

## Procedimiento. 

## Fase 1. 

Analisis: Definir las variables que seran monitoreadas con arduinos, definir las areas o puntos  de estudio
donde se colocaran los arduinos para primera toma de datos para establecer configuraciones, protocolos para el levantamientos de datos 


## Fase 2. 

Instalacion: Colocar los sensores en las areas de cultivo. 

## Fase 3.

Monitoreo: Seguimiento a toma de datos de los sensores. 

## Fase 4. 





## 1era. Fase semana uno 

1. Determinar que sensores son los mas adecuados. 
1. Determinar areas para las pruebas.
1. Redactar la justificacion de las variables.
1. Primer listado de las variables con los valores de las areas de estudio.
1. Analisar la informacion y definir las estrategias para la siguiente fase.





# Justificacion de las variables. 

Si deseamos obtener plantas de buena calidad, plantas que logren producir a su maxima capacidad debemos darles a estas las mejores
condiciones. Algunos de estas nosotros podemos darselos directamente otros dependen del ambiente donde
las instalemos. 
Son de interes para este estudio medir y registrar varias condiciones ambientales. 

Humedad: Ent


Temperatura:


luz:













